/*
   Copyright 2020 Bota Viorel

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

use std::{thread};
use logger::log;

mod configurations;
use configurations::settings::Settings;
use crate::configurations::settings;
use crate::configurations::settings::Sensor;

use data_formats::alive_message::AliveMessage;
use data_formats::Markers;

fn main() {
    println!("Copyright 2020 Bota Viorel. Distributed under the Apache License, Version 2.0");

    let settings_left_side_sensor = match Settings::from_file(settings::LEFT_SENSOR_CONFIG.to_string()) {
        Ok(value) => value,
        Err(error) => {
            log::fatal(error);
            return;
        }
    };

    let settings_right_side_sensor = match Settings::from_file(settings::RIGHT_SENSOR_CONFIG.to_string()) {
        Ok(value) => value,
        Err(error) => {
            log::fatal(error);
            return;
        }
    };

    let left_side_collector = loop {
        let sensor = settings_left_side_sensor.sensor.clone();
        let (stop_monitoring, monitoring_ended) = mio_extras::channel::channel();
        let (process_message, message_received) = std::sync::mpsc::channel();

        match connect_sensor(sensor, monitoring_ended, process_message, &message_received) {
            Ok(_) => {},
            Err(error) => {
                log::error(format!("Connection to left side sensor failed. {}", error));
                continue;
            }
        }

        let settings = settings_left_side_sensor.clone();
        break thread::spawn(|| { collect_data(settings, stop_monitoring, message_received); });
    };


    let right_side_collector = loop {
        let sensor = settings_right_side_sensor.sensor.clone();
        let (stop_monitoring, monitoring_ended) = mio_extras::channel::channel();
        let (process_message, message_received) = std::sync::mpsc::channel();

        match connect_sensor(sensor, monitoring_ended, process_message, &message_received){
            Ok(_) => {},
            Err(error) => {
                log::error(format!("Connection to left side sensor failed. {}", error));
                continue;
            }
        }

        let settings = settings_right_side_sensor.clone();
        break thread::spawn(|| { collect_data(settings, stop_monitoring, message_received); });
    };

    left_side_collector.join().ok();
    right_side_collector.join().ok();

}

fn connect_sensor(
    sensor: Sensor,
    monitoring_ended: mio_extras::channel::Receiver<bool>,
    process_message: std::sync::mpsc::Sender<Vec<u8>>,
    new_message_received: &std::sync::mpsc::Receiver<Vec<u8>>)
    -> Result<(), String>
{
    let address = sensor.address.clone();
    thread::spawn(|| {
        data_receiver::start_acquisition(
            sensor.name,
            sensor.address,
            sensor.timeout,
            process_message,
            monitoring_ended);
    });

    let message: Vec<u8> = match new_message_received.recv() {
        Ok(message) => message,
        Err(error) => {
            return Err(format!("Error while connecting with device {:?}. {}", address, error));
        }
    };

    #[cfg(debug_assertions)] {
        return Ok(());
    }

    if message != AliveMessage::get_start_symbol() {
        return Err(format!("Received unexpected connection validation symbol {:?}", message))
    }

    Ok(())
}

fn collect_data(settings: Settings, stop_listening: mio_extras::channel::Sender<bool>, new_message_received: std::sync::mpsc::Receiver<Vec<u8>>) {
    loop {
        let message: Vec<u8> = match new_message_received.recv() {
            Ok(message) => message,
            Err(_) => {
                log::trace(format!("No more data expected from device {:?}", settings.sensor.address));
                break;
            }
        };

        let database_clone = settings.database.clone();
        thread::spawn(move || {
            database_handler::store(
                database_clone.uri,
                database_clone.name,
                database_clone.collection,
                message
            );
        });
    }
    stop_listening.send(true).ok();
}
