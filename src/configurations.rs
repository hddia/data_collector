/*
   Copyright 2020 Bota Viorel

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

pub mod settings {
    use std::fs;
    use serde_derive::Deserialize;

    pub(crate) const LEFT_SENSOR_CONFIG: &str = "configuration_left_sensor.json";
    pub(crate) const RIGHT_SENSOR_CONFIG: &str = "configuration_right_sensor.json";

    #[derive(Deserialize)]
    #[derive(Clone)]
    pub(crate)  struct Settings{
        pub(crate) database: Database,
        pub(crate) sensor: Sensor
    }

    #[derive(Deserialize)]
    #[derive(Clone)]
    pub(crate) struct Database
    {
        pub(crate) uri: String,
        pub(crate) name:  String,
        pub(crate) collection: String
    }

    #[derive(Deserialize)]
    #[derive(Clone)]
    pub(crate) struct Sensor
    {
        pub(crate) position: String,
        pub(crate) name: String,
        pub(crate) address: Vec<u8>,
        pub(crate) timeout: u64
    }

    impl Settings {
        pub(crate) fn from_file(file: String) -> Result<Settings, String> {
            let file_content = match fs::read_to_string(file){
                Ok(data) => data,
                Err(error) => return Err( format!("Couldn't open configuration file. {}", error))
            };

            let settings: Settings = match serde_json::from_str(file_content.as_str()){
                Ok(value) => value,
                Err(error) => return Err( format!("Could not interpret config file content as json. {}", error))
            };

            Ok(settings)
        }
    }
}